# argocd-jenkins-using-helm


1. kubectl create namespace argocd
2. kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

3. kubectl port-forward svc/argocd-server -n argocd 8080:443

4. login - admin / pwd 
    4a. Get the password, open linux bash terminal and run (kubectl -n argocd get secret argocd-initial-admin-secret -o yaml)
    echo VThYMFBTM1EzbjlPWTNuOA== | base64 --decode
    enter the password you decoded

5. kubectl apply -f application.yaml

6. kubectl get pod -n jenkins
   kubectl port-forward -n jenkins jenkins-6c78d94856-7nrdh 3000:8080

7. kubectl -n jenkins exec -it jenkins-67dd967f67-hhj8d cat /var/jenkins_home/secrets/initialAdminPassword

8.  userid=jenkins_user_ag and pwd= AGisADmin
    fullname= Jenkins User |  email=Jenkins@user.jenkins | url= http://localhost:8080/

9.  Go to Manage Jenkins | Bottom of Page | Cloud | Kubernetes (Add kubenretes cloud)
    Fill out plugin values
    Name: kubernetes
    Kubernetes URL: https://kubernetes.default:443
    Kubernetes Namespace: jenkins
    Credentials | Add | Jenkins (Choose Kubernetes service account option & Global + Save)
    Test Connection | Should be successful! If not, check RBAC permissions and fix it!
    Jenkins URL: http://jenkins
    Tunnel : jenkins:50000
    Apply cap only on alive pods : yes!
    Add Kubernetes Pod Template
    Name: jenkins-slave
    Namespace: jenkins
    Service Account: jenkins
    Labels: jenkins-slave (you will need to use this label on all jobs)
    Containers | Add Template
    Name: jnlp
    Docker Image: aimvector/jenkins-slave
    Command to run :
    Arguments to pass to the command:
    Allocate pseudo-TTY: yes
    Add Volume
    HostPath type
    HostPath: /var/run/docker.sock
    Mount Path: /var/run/docker.sock
    Timeout in seconds for Jenkins connection: 300
    Save

10. Create simple pipeline to test 
    Select pipeline script from SCM 
    select git
    enter github repo https://github.com/addisuICS325/jenkins
    change branch specification to MAIN
    makes sure Jenkinsfile 
11. Testing prune 
  name: myapp-argo-application 
  namespace: argocd
spec:
  project: default

  source:
    repoURL: https://gitlab.com/devops1400/argocd-jenkins-using-helm.git